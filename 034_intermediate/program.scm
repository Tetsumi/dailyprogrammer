;;-----------------------------------------------------------------------------
;; [2012-12-31] Challenge #34 [Intermediate]
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (bogosort l pred?)
  (define v (list->vector l))
  (define length (vector-length v))
  
  (define (shuffle! i)
    (when (< i length)
      (let* ([slot (random length)]
	     [vala (vector-ref v i)]
	     [valb (vector-ref v slot)])
	(vector-set! v i valb)
	(vector-set! v slot vala)
	(shuffle! (add1 i)))))
  
  (define (iter!)
    (if (sorted? v pred?)
	(vector->list v)
	(begin
	  (shuffle! 0)
	  (iter!))))
  
  (iter!))

(define (stoogesort l pred?)
  (define v (list->vector l))
  (define end (sub1 (vector-length v)))
  
  (define (stooge! start end)
    (define sv (vector-ref v start))
    (define ev (vector-ref v end))

    (when (pred? ev sv)
      (vector-set! v start ev)
      (vector-set! v end sv))
    (let* ([rest (add1 (- end start))]
	   [tier (quotient rest 3)]
	   [nend (- end tier)])
      (when (> rest 2)
	(stooge! start nend)
	(stooge! (+ start tier) end)
	(stooge! start nend))))

  (stooge! 0 end)
  (vector->list v))
    
(define target '(1231 4 1 808 -234 -11 0 1))
(printf "Target := ~A~%" target)
(printf "(Bogosort Target >) => ~A~%" (bogosort target >))
(printf "(Stoogesort Target <) => ~A~%" (stoogesort target <))
