;;-----------------------------------------------------------------------------
;; [2015-07-20] Challenge #224 [Easy]
;; Shuffling a List
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------
(use format)

(define (shuffle l)
  (define v (list->vector l))
  (define length (vector-length v))
  
  (define (iter! i)
    (when (< i length)
      (let* ([slot (random length)]
	     [vala (vector-ref v i)]
	     [valb (vector-ref v slot)])
	(vector-set! v i valb)
	(vector-set! v slot vala)
	(iter! (add1 i)))))
  
  (iter! 0)
  (vector->list v))

(format #t "~{~A ~}~%" (shuffle (string-split (read-line))))
