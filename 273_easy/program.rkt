;;-----------------------------------------------------------------------------
;; [2016-06-27] Challenge #273 [Easy]
;; Getting a degree
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   RACKET
;;-----------------------------------------------------------------------------
#lang racket

(define (convert s)
  (define (toString n p)
    (string-append (~r n #:precision 2) p))
  
  (match (regexp-match #rx"([-0-9.]+)([A-Za-z])([A-Za-z])" s)
    [(list _ n "d" "r") (toString (degrees->radians (string->number n))    "r")]
    [(list _ n "r" "d") (toString (radians->degrees (string->number n))    "d")]
    [(list _ n "c" "f") (toString (+ (* (string->number n)    9/5)     32) "f")]
    [(list _ n "c" "k") (toString (+ (string->number n)            273.15) "k")]
    [(list _ n "f" "c") (toString (* (- (string->number n)     32)    5/9) "c")]
    [(list _ n "f" "k") (toString (* (+ (string->number n) 459.64)    5/9) "k")]
    [(list _ n "k" "c") (toString (- (string->number n)            273.15) "c")]
    [(list _ n "k" "f") (toString (- (* (string->number n)    9/5) 459.64) "f")]
    [_ "No candidate for conversion"]))

(for-each (lambda (s) (displayln (convert s)))
          (list "3.1416rd"
                "90dr"
                "212fc"
                "70cf"
                "100cr"
                "315.15kc"))
