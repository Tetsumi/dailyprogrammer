;;-----------------------------------------------------------------------------
;; [2015-05-22] Challenge #215 [Hard]
;; Metaprogramming Madness!
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use format)

(define (table . x)
  (define (type-of x)
    (cond
     [(boolean? x) "Boolean"]
     [(blob? x) "Blob"]
     [(pair? x) "Pair"]
     [(fixnum? x) "Fixnum"]
     [(integer? x) "Integer"]
     [(symbol? x) "Symbol"]
     [(real? x) "Float"]
     [(number? x) "Number"]     
     [(null? x) "Empty list"]
     [(list? x) "List"]
     [(char? x) "Char"]
     [(string? x) "String"]
     [(eof-object? x) "End-of-file"]     
     [(pointer? x) "Pointer"]))
          
  (define (iter l)
    (when (not (null? l))
      (format #t  "~15@A | ~15@A | ~A\n"
	      (car l)
	      (type-of (car l))
	      (if (not (not (car l))) "True" "False"))
      (iter (cdr l))))

  (iter x))

(table
 1
 0
 -1
 '()
 4
 4.0
 #f
 #t
 '(1 2 3)
 #!eof
 '(4 . 1)
 'foo
 #\c
 "Foobar")
