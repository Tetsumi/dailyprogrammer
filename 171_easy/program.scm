;;-----------------------------------------------------------------------------
;; [2014-07-07] Challenge #171 [Easy]
;; Hex to 8x8 Bitmap
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use format)

(define (get-inputlines)
  (define (iter l)
    (let ([i (read-line)])
      (if (eof-object? i)
	  (reverse l)
	  (iter (cons i l)))))
  (iter '()))

(define (challenge l)
  (define hexlist (map (lambda (x) (string->number x 16)) l))
  (for-each
   (lambda (x)
     (display (string-translate (format #f "~8b~%" x) "10" "# ")))
   hexlist)  
  (newline))

(for-each challenge (map string-split (get-inputlines)))

