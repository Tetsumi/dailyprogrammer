;;-----------------------------------------------------------------------------
;; [2012-03-08] Challenge #85 [Hard]
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------


(define (b-add x y)
  (cons (fxxor x y) (fxand x y)))

(define (b-add-carry x y carry)
  (let* ([xc (b-add x carry)]
	 [cxc (cdr xc)]
	 [yxc (b-add y (car xc))])
    (cons (car yxc) (fxior (cdr yxc) cxc))))

(define (b-nth x nth)
  (fxand (fxshr x nth) 1))

(define (b-add-nth-carry x y nth carry)
  (b-add-carry (b-nth x nth) (b-nth y nth) carry))

(define (add x y)
  (define (iter i r carry)
    (if (>= i 63)
	r
	(let ([c (b-add-nth-carry x y i carry)])
	  (iter (add1 i) (fxior r (fxshl (car c) i)) (cdr c)))))
  (iter 0 0 0))

(define (sub x y)
  (add x (neg y)))

(define (neg x)
  (fxnot (sub1 x)))

(define (mul x y)
  (define sb (neg 1)) 
  (define (iter r i)
    (if (= i 0)
	r
	(iter (add r x) (add i sb))))
  (when (< y 0)
    (set! y (neg y))
    (set! x (neg x)))
  (iter 0 y))

(define (div-mod x y)
  (define (iter i q)
    (if (< i y)
	(cons q i)
	(iter (sub i y) (add q 1))))
  (iter x 0))

(define (div x y)
  (car (div-mod x y)))

(define (mod x y)
  (cdr (div-mod x y)))

(define (exp x y)
  (define (iter r i)
    (if (= i 0)
	r
	(iter (mul r x) (sub i 1))))
  (iter 1 y))

(define (f a b c d e)
  (mul (mod a e)
       (add (div b (sub c a))
	    (exp (mul d (neg b)) e))))

(printf "~A~%" (f 50 -40 300 2 3))
(exit)
