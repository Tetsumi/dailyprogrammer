;;-----------------------------------------------------------------------------
;; [2015-03-23] Challenge #207 [Easy]
;; Bioinformatics 1: DNA Replication
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use srfi-13)

(define (inverter x)
  (cond
   [(char=? x #\T) #\A]
   [(char=? x #\A) #\T]
   [(char=? x #\G) #\C]
   [(char=? x #\C) #\G]
   [else x]))

(define (print-codonizer l)
  (define (nextstep x c1 c2 c3 c4 default)
    (cond
     [(char=? x #\T) c1]
     [(char=? x #\A) c2]
     [(char=? x #\G) c3]
     [(char=? x #\C) c4]
     [else default]))

  (define (TCorAG l tc ag)
    (if (null? l)
	(print-codonizer l)
	(begin
	  (printf "~A " (nextstep (car l) tc ag  ag tc "ERROR"))
	  (print-codonizer (cdr l)))))

  (define (TCAG l tcag)
    (if (null? l)
	(print-codonizer l)
	(begin
	  (printf tcag)
	  (print-codonizer (cdr l)))))

  (define (AT l)
    (if (null? l)
	(print-codonizer l)
	(begin
	  (printf "~A " (if (char=? (car l) #\G) "Met" "Ile"))
	  (print-codonizer (cdr l)))))

  (define (AC l) (TCAG l "Thr "))
  (define (AA l) (TCorAG l "Asn" "Lys"))
  (define (AG l) (TCorAG l "Ser" "Arg"))
  (define (TT l) (TCorAG l "Phe" "Leu"))
  (define (TA l) (TCorAG l "Tyr" "Stop"))
  (define (TC l) (TCAG l "Ser "))
  (define (CT l) (TCAG l "Leu "))
  (define (CC l) (TCAG l "Pro "))
  (define (CG l) (TCAG l "Arg "))
  (define (CA l) (TCorAG l "His" "Gln"))
  (define (GT l) (TCAG l "Val "))
  (define (GC l) (TCAG l "Ala "))
  (define (GA l) (TCorAG l "Asp" "Glu"))
  (define (GG l) (TCAG l "Gly "))
    
  (define (TG l)
    (if (null? l)
	(print-codonizer l)
	(begin
	  (printf "~A " (nextstep (car l)  "Cys" "Stop"  "Trp" "Cys" "ERROR"))
	  (print-codonizer (cdr l)))))
   
  (define (A l) (if (null? l)
		    (print-codonizer l)
		    ((nextstep (car l) AT AA AG AC A) (cdr l))))
   
  (define (T l) (if (null? l)
		    (print-codonizer l)
		    ((nextstep (car l) TT TA TG TC T) (cdr l))))
  
  (define (G l) (if (null? l)
		    (print-codonizer l)
		    ((nextstep (car l) GT GA GG GC G) (cdr l))))
  
  (define (C l) (if (null? l)
		    (print-codonizer l)
		    ((nextstep (car l) CT CA CG CC C) (cdr l))))
   
  (if (null? l)
      (newline)
      ((nextstep (car l) T A G C print-codonizer) (cdr l))))

(define (get-inputlines)
  (define (iter l)
    (let ([i (read-line)])
      (if (eof-object? i)
	  (reverse l)
	  (iter (cons i l)))))
  (iter '()))

(define (challenge line)
  (printf "~A~%" line)
  (printf "~A~%" (string-map inverter line))
  (print-codonizer (string->list (string-filter char-alphabetic? line))))

(for-each challenge (get-inputlines))
