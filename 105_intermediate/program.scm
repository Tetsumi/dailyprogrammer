;;-----------------------------------------------------------------------------
;; [2012-10-20] Challenge #105 [Intermediate]
;; Boolean logic calculator
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(require-extension regex)

(define (compute s)
  (define tokens (string-split-fields "(\\d+|\\!|\\^|\\*|\\|)" s))

  (define (pred x)
    (cond [(string=? x "!") 1]
	  [(string=? x "|") 2]
	  [(string=? x "*") 4]
	  [(string=? x "^") 3]))

  (define (op o r)
    (cond [(string=? o "!") (cons (fxnot (car r)) (cdr r))]
	  [(string=? o "|") (cons (fxior (car r) (cadr r)) (cddr r))]
	  [(string=? o "*") (cons (fxand (car r) (cadr r)) (cddr r))]
	  [(string=? o "^") (cons (fxxor (car r) (cadr r)) (cddr r))]
	  [else r]))

  (define (handlestack o r)
    (if (null? o)
	r
	(handlestack (cdr o) (op (car o) r))))
  
  (define (iter s o r)
    (if (null? s)
	(handlestack o r)
	(let* ([token (car s)]
	       [n (string->number token 2)])
	  (cond [n (iter (cdr s) o (cons n r))]
		[(or (null? o)
		     (< (pred token) (pred (car o))))
		 (iter (cdr s) (cons token o) r)]
		[else (iter s (cdr o) (op (car o) r))]))))
  
  (car (iter tokens '() '())))

(format #t "~B~%" (compute (read-line)))
