;;-----------------------------------------------------------------------------
;; [2015-04-17] Challenge #210 [Hard]
;; Loopy Robots
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (check-if-loop l)
  (define (iter l p o)
    (if (null? l)
	(if (and (= o 0) (not (and (= (car p) 0) (= (cdr p) 0))))
	    (printf "No loop detected!~%")
	    (printf
	     "Loop detected! ~A cycle(s) to complete loop~%"
	     (cond [(= o 0) 1]
		   [(= o 2) 2]
		   [else 4])))
	(let ([step (car l)]
	      [x (car p)]
	      [y (cdr p)])
	  (if (char=? step #\S)
	      (iter (cdr l)
		    (if (even? o)
			(cons x ((if (= 0 o) - +) y 1))
			(cons ((if (= 1 o) - +) x 1) y))
		    o)
	      (iter (cdr l) p (modulo ((if (char=? step #\R) - +) o 1) 4))))))
  (iter l (cons 0 0) 0))

(define (get-inputlines)
  (define (iter l)
    (let ([i (read-line)])
      (if (eof-object? i)
	  l
	  (iter (cons i l)))))
  (iter '()))

(for-each check-if-loop (map string->list (reverse (get-inputlines))))
