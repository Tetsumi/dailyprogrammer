;;-----------------------------------------------------------------------------
;; [2015-06-01] Challenge #217 [Intermediate]
;; Space Code Breaking
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use srfi-1)

(define (get-inputlines)
  (define (iter l)
    (let ([i (read-line)])
      (if (eof-object? i)
	  l
	  (iter (cons i l)))))
  (iter '()))

(define (parse-lines l)
  (define (parse x)
    (filter-map string->number (string-split x)))
  
  (define (iter lx ly)
    (if (null? lx)
	ly
	(iter (cdr lx) (cons (parse (car lx)) ly))))
  (iter l '()))

(define (cusmap x)
  (define (omicron->char x)
    (integer->char (bitwise-xor x #b10000)))
  
  (define (hoth->char x)
    (integer->char (- x 10)))
  
  (define (ryza->char x)
    (integer->char (+ x 1)))
  
  (define (cpred? x)
    (or (char-alphabetic? x) (char-whitespace? x)))

  (let ([omi (map omicron->char x)]
	[hoth (map hoth->char x)]
	[ryza (map ryza->char x)])
    (cond [(every cpred? omi) omi]
	  [(every cpred? hoth) hoth]
	  [(every cpred? ryza) ryza]
	  [else (map integer->char (reverse x))])))
    
(define (cusprint x)
  (printf "~A\n" (list->string x)))

(define sentences (parse-lines (get-inputlines)))

(for-each cusprint (map cusmap sentences))

