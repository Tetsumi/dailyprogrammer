;;-----------------------------------------------------------------------------
;; [2012-05-16] Challenge #53 [Intermediate]
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use srfi-1)

(define seed 123456789)

(define (rand)
  (set! seed (modulo (+ 12345 (* seed 22695477)) 1073741824))
  seed)

(define rnlist (list-tabulate 10000000 (lambda (x) (rand))))

(printf "~A~%" (apply + (take (sort rnlist >) 1000)))

