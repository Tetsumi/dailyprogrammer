;;-----------------------------------------------------------------------------
;; [2014-06-09] Challenge #166 [Easy]
;; Fractal Curves
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use numbers)
(use srfi-25)

(define (fractal n z)
  (define (iter n l)
    (if (<= n 0)
	l
	(iter (sub1 n)
	      (cons ((if (even? (random 10000)) + -)
		     1.0
		     (* (car l) z)) l))))
  (iter n '(0)))

(define buffer-width 64)
(define buffer-height 32)
(define buffer (make-array (shape 0 buffer-width 0 buffer-height) #\space))

(define (buffer-set-xy! x y val)
  (when (not (or (>= x buffer-width)
		 (< x 0)
		 (>= y buffer-height)
		 (< y 0)))
    (array-set! buffer x y val)))

(define (buffer-print)
  (define (iter-lines y)
    (define (iter-columns x)
      (when (< x buffer-width)
	(display (array-ref buffer x y))
	(iter-columns (add1 x))))
    
    (when (< y buffer-height)
      (iter-columns 0)
      (newline)
      (iter-lines (add1 y))))
  (iter-lines 0))

(define (ftb o)
  (define scaled-o (* o 32))
  (buffer-set-xy!
   (inexact->exact (round (real-part scaled-o)))
   (inexact->exact (round (imag-part scaled-o))) #\X))


(for-each ftb (fractal (read) 0.355-0.588i))
(buffer-print)

