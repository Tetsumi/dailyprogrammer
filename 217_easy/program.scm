;;-----------------------------------------------------------------------------
;; [2015-06-01] Challenge #217 [Easy]
;; Lumberjack Pile Problem
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   Guile
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (parse-log-piles n)
  (define (iter x l)
    (if (= x n)
	l
	(iter (+ x 1) (cons (cons x (read)) l))))
  (iter 0 '()))

(define (sort-logsamount l)
  (sort l (lambda (x y)
	    (< (cdr x) (cdr y)))))

(define (sort-position l)
  (sort log-piles (lambda (x y)
		    (< (car x) (car y)))))

(define (distribute-logs! l nlogs)
  (define (inc-log! ll)
    (set-cdr! (car ll) (+ (cdar ll) 1)))
  
  (when (> nlogs 0)
	(begin
	  (inc-log! l)
	  (distribute-logs! (sort-logsamount l) (- nlogs 1)))))

(define (print-log-piles l rowsize)
  (define (iter x l)
    (cond
     [(null? l) (newline)]
     [(= x rowsize) (begin
		      (newline)
		      (iter 0 l))]
     [else (begin
	     (format #t "~a " (cdr (car l)))
	     (iter (+ x 1) (cdr l)))]))

  (iter 0 l))

(define matrix-size (read))
(define number-of-logs (read))
(define log-piles (sort-logsamount
		   (parse-log-piles (* matrix-size matrix-size))))

(distribute-logs! log-piles number-of-logs)
(print-log-piles (sort-position log-piles) matrix-size)
