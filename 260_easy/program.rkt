;-----------------------------------------------------------------------------
; [2016-03-28] Challenge #260 [Easy]
; Garage Door Opener
;
; Tetsumi <tetsumi@vmail.me>
;
; Compatible with
;  RACKET
;-----------------------------------------------------------------------------

#lang racket

(define (next onButton onCycle)
  (case (read-line)
    [("button_clicked")
     (displayln "> Button clicked.")
     (onButton)]
     [("cycle_complete")
      (displayln "> Cycle complete.")
      (onCycle)]))

(define-syntax-rule (la msg onButton onCycle)
  (lambda ()
    (displayln msg)
    (next onButton onCycle)))
  
(define (cycleError) (error "No cycle to terminate."))

(define stopClosing (la "Door: STOPPED_WHILE_CLOSING" opening     cycleError))
(define closing     (la "Door: CLOSING"               stopClosing closed))
(define closed      (la "Door: CLOSED"                opening     cycleError))
(define stopOpening (la "Door: STOPPED_WHILE_OPENING" closing     cycleError))
(define opening     (la "Door: OPENING"               stopOpening opened))
(define opened      (la "Door: OPEN"                  closing     cycleError))

(closed)
