;;-----------------------------------------------------------------------------
;; [2015-06-22] Challenge #220 [Easy]
;; Mangling sentences 
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (special-sort! l)
  (define (special-min l)
    (define (valid-chars? x y)
      (and
       (or (char-alphabetic? x)
	   (char-numeric? x))
       (or (char-alphabetic? y)
	   (char-numeric? y))))
    
    (define (iter l min)
      (cond
       [(null? l) min]
       [(and
	 (valid-chars? (car l) (car min))
	 (char-ci>? (car min) (car l)))
	(iter (cdr l) l)]
       [else (iter (cdr l) min)]))
    
    (iter (cdr l) l))
  
  (define (to-same-case x y)
    ((if (char-upper-case? x)
	 char-upcase
	 char-downcase)
     y))
  
  (when (not (null? (cdr l)))
	(let ([min (special-min l)]
	      [cur (car l)])
	    (when (char-ci>? cur (car min))
		(let ([casecur (to-same-case (car min) cur)]
		      [casemin (to-same-case cur (car min))])
		  (set-car! l casemin)
		  (set-car! min casecur))))
	(special-sort! (cdr l)))
  l)
  
(define (printwords l)
  (when (not (null? l))
	(format #t "~A " (car l))
	(printwords (cdr l))))

(printwords (map (lambda (x)
		   (list->string (special-sort! (string->list x))))
		 (string-split (read-line))))
