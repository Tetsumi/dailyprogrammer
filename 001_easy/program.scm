;;-----------------------------------------------------------------------------
;; Challenge #1 [Easy]
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (question caption)
  (printf caption)
  (read-line))

(define outputstring
  (format
   #f
   " your name is ~A, you are ~A years old, and your username is ~A~%"
   (question "Enter your name: ")
   (question "Enter your age: ")
   (question "Enter your username: ")))

(display outputstring)
(with-output-to-file "log.txt" (lambda () (format #t outputstring)))
