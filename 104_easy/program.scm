;;-----------------------------------------------------------------------------
;; [2012-10-18] Challenge #104 [Easy]
;; Powerplant Simulation
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (simulation n)
  (+ n
     (- 0
	(quotient n 3)
	(quotient n 14)
	(quotient n 100)
	(quotient n 2100))
     (quotient n 42)
     (quotient n 700)
     (quotient n 300)))
          
(for-each
 (lambda (o) (printf "~A~%" (simulation o)))
 '(10 100 548 69784 124785))
