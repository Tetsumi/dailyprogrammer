;;-----------------------------------------------------------------------------
;; [2016-06-29] Challenge #273 [Intermediate]
;; Twist up a message
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   RACKET
;;-----------------------------------------------------------------------------
#lang racket

(define diacritics #("aáăắặằẳẵǎâấậầẩẫäạàảāąåǻãɑɐɒȃǡȧȁǟ"
                     "bḅɓß♭␢Б"
                     "cćčçĉɕċ"
                     "dďḓḍɗḏđɖᶑ"
                     "eéĕěêếệềểễëėẹèẻēęẽɘəɚḛɇȅḕ"
                     "fƒſʃʆʅɟʄᶂḟ"
                     "gǵğǧģĝġɠḡɡᶃ"
                     "hḫĥḥɦẖħɧḣⱨḩȟ"
                     "iíĭǐîïịìỉīįɨĩɩıẛᵻ"
                     "jǰĵʝȷɟʄ"
                     "kķḳƙḵĸʞ"
                     "lĺƚɬľļḽḷḹḻŀɫɭłᶅ"
                     "mḿṁṃɱɯɰ"
                     "nŉńňņṋṅṇǹɲṉɳñŋȵ"
                     "oóŏǒôốộồổỗöọőòỏơớợờởỡōǫøǿõɵʘȱ"
                     "pɸþᵱƥᵽṗṕᶈ"
                     "qʠꝗɋq̃ϙ"
                     "rŕřŗṙṛṝɾṟɼɽɿɹɻᵲ"
                     "sśšşŝșṡṣʂṧᶊȿṥṩ"
                     "tťţṱțẗṭṯʈŧᵵƭƫȶṫⱦ"
                     "uʉúŭǔûüǘǚǜǖụűùủưứựừửữūųůũʊ"
                     "vʋʌⱴṿṽ"
                     "wẃŵẅẁʍẘẉ"
                     "xχẍẋⲭᶍ"
                     "yýŷÿẏỵỳƴỷȳỹʎʏ"
                     "zźžʑżẓẕʐƶ"
                     "AÁĂẮẶẰẲẴǍÂẤẬẦẨẪÄẠÀẢĀĄÅǺÃ"
                     "BḄƁᛒ𐌱ɃḂḆ฿β"
                     "CĆČÇĈĊƆʗ"
                     "DĎḒḌƊḎĐÐ"
                     "EÉĔĚÊẾỆỀỂỄËĖẸÈẺĒĘẼƐ"
                     "FƑḞ𐌅₣"
                     "GǴĞǦĢĜĠḠʛ"
                     "HḪĤḤĦ"
                     "IÍĬǏÎÏİỊÌỈĪĮĨ"
                     "JĴɈʝ"
                     "KĶḲƘḴ"
                     "LĹȽĽĻḼḶḸḺĿŁ"
                     "MḾṀṂ"
                     "NŃŇŅṊṄṆǸƝṈÑ"
                     "OÓŎǑÔỐỘỒỔỖÖỌŐÒỎƠỚỢỜỞỠŌƟǪØǾÕ"
                     "PÞ𐌐ṔṖⱣƤ₱♇"
                     "QꝖɊ"
                     "RŔŘŖṘṚṜṞʁ"
                     "SŚŠŞŜȘṠṢ"
                     "TŤŢṰȚṬṮŦ"
                     "UÚŬǓÛÜǗǙǛǕỤŰÙỦƯỨỰỪỬỮŪŲŮŨ"
                     "VṼṾƲ℣∨"
                     "WẂŴẄẀʬ"
                     "XχẌẊⲬ𐍇"
                     "YÝŶŸẎỴỲƳỶȲỸ"
                     "ZŹŽŻẒẔƵ"))

(define (twist str f)
  (for ([char (in-string str)])
    (define dia (for/first ([e (in-vector diacritics)]
                            #:when (member char (string->list e)))
                  e))
    (printf "~A" (if dia (f char dia) char)))
  (newline))

(define (twistUp c str)
  (define rc (list-ref (string->list str) (random 1 (string-length str))))
  (if (char=? c rc)
      (twistUp c str)
      rc))

(define (twistDown c str)
  (substring str 0 1))

(for-each
 (lambda (x) (twist x twistUp))
 '("For, after all, how do we know that two and two make four? "
   "Or that the force of gravity works? Or that the past is unchangeable? "
   "If both the past and the external world exist only in the mind, "
   "and if the mind itself is controllable – what then?"
   ""
   "Dżdżystym rankiem gżegżółki i piegże, zamiast wziąć się za dżdżownice,"
   "nażarły się na czczo miąższu rzeżuchy i rzędem rzygały do rozżarzonej brytfanny."))

(newline)

(for-each
 (lambda (x) (twist x twistDown))
 '("Ṭħë ᶈṝộȱƒ țḣẵţ ƭĥề ɬıṭᵵḷḛ ᵱᵲíȵċɇ ɇxẛṣⱦėḏ ɨś ƫḥẳṯ ħė ẘắś ĉⱨȃṟḿíņğ, ƫħằṫ ĥḛ ᶅẫủᶃḩëᶑ,"
   "áñɗ ţḥầť ḫẻ ẉâṧ łỗǫḳĩņğ ᶂờŕ ầ ᶊĥȅẹᵽ. Īḟ ǡɲÿɓộđʏ ẁȧṉȶȿ â ȿĥểêᵱ, ⱦḣąʈ ᵻṥ ȁ ᵱṟỗǒƒ ṫȟǟṭ ḫĕ ḕᶍĭṩťș."))
