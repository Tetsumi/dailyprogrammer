;;-----------------------------------------------------------------------------
;; [2012-10-18] Challenge #104 [Intermediate]
;; Bracket Racket
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(define (parse s)
  (define (f x)
    (cond [(char=? x #\() #\)]
	  [(char=? x #\{) #\}]
	  [(char=? x #\[) #\]]
	  [(char=? x #\<) #\>]
	  [else #f]))

  (define (iter l s)
    (if (null? l)
	(null? s)
	(let* ([lh (car l)]
	       [lt (cdr l)]
	       [c (f lh)])
	  (cond [c (iter lt (cons c s))]
		[(or (char=? lh #\))
		     (char=? lh #\})
		     (char=? lh #\])
		     (char=? lh #\>))
		 (if (or (null? s)
			 (not (char=? lh (car s))))
		     #f
		     (iter lt (cdr s)))]
		[else (iter lt s)]))))

  (iter (string->list s) '()))
