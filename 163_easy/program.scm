;;-----------------------------------------------------------------------------
;; [2014-05-19] Challenge #163 [Easy]
;; Probability Distribution of a 6 Sided Di
;; 
;; Tetsumi <tetsumi@vmail.me>
;;
;; Compatible with
;;   CHICKEN
;;-----------------------------------------------------------------------------

(use format)

(define results (make-vector 6 0))

(define (iter x)
  (when (> x 0)
      (let* ([slot (random 6)]
	     [previous (vector-ref results slot)])
	(vector-set! results slot (add1 previous))
	(iter (sub1 x)))))

(define (print-line a b c d e f g)
  (format #t
   "~6A | ~6A | ~6A | ~6A | ~6A | ~6A | ~6A~%"
   a b c d e f g))

(define (get-percent slot f)
  (/ (vector-ref results slot) (/ f 100.0)))

(define (challenge f t)
  (when (<= f t)
    (iter f)
    (print-line
     f
     (get-percent 0 f)
     (get-percent 1 f)
     (get-percent 2 f)
     (get-percent 3 f)
     (get-percent 4 f)
     (get-percent 5 f))
    (challenge (* f 10) t)))

(print-line "Rolls" "1" "2" "3" "4" "5" "6")
(challenge 1 100000)
	  
      
